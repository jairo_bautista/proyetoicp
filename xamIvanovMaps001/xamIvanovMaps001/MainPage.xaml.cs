﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Essentials;
using Xamarin.Forms.Maps;
using Plugin.Geolocator;

namespace xamIvanovMaps001
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        double dblUsuarioLat = 0;
        double dblUsuarioLon = 0;

        public MainPage()
        {
            InitializeComponent();
            obtenerUbicacion();
            seguirUbicacion();
        }

        private async void obtenerUbicacion()
        {
            try
            {
                mapMapa.IsShowingUser = true;
                var request = new GeolocationRequest(GeolocationAccuracy.Medium);
                Location location = await Geolocation.GetLocationAsync(request);

                if (location != null)
                {
                    dblUsuarioLat = location.Latitude;
                    dblUsuarioLon = location.Longitude;
                    Position position = new Position(dblUsuarioLat, dblUsuarioLon);
                    mapMapa.MoveToRegion(new MapSpan(position, 0.01, 0.01));
                }
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
            }
            catch (FeatureNotEnabledException fneEx)
            {
                // Handle not enabled on device exception
            }
            catch (PermissionException pEx)
            {
                // Handle permission exception
            }
            catch (Exception ex)
            {
                // Unable to get location
            }

        }

        private async void seguirUbicacion()
        {
            var locator = CrossGeolocator.Current;
            locator.DesiredAccuracy = 50;
            if(locator.IsGeolocationAvailable)
            {
                if(locator.IsGeolocationEnabled)
                {
                    if(locator.IsListening)
                    {
                        //var position = await locator.GetPositionAsync();
                        await locator.StartListeningAsync(new TimeSpan(0, 0, 5), 20);
                    }
                    locator.PositionChanged += (cambio, args) =>
                    {
                        var loc = args.Position;
                        Position position = new Position(loc.Latitude, loc.Longitude);
                        mapMapa.MoveToRegion(new MapSpan(position, 0.01, 0.01));
                    };
                }
            }
        }
    }
}
