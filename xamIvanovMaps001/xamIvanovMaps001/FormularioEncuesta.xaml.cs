﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using xamIvanovMaps001.Data;
using xamIvanovMaps001.Models;

namespace xamIvanovMaps001
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FormularioEncuesta : ContentPage
    {
        public FormularioEncuesta()
        {
            InitializeComponent();
        }

        WebApiHelper webApiHelper = new WebApiHelper();

        void switcher_Toggled14DSintomas(object sender, ToggledEventArgs e)
        {
            if (Encuesta14dNingunoXaml.IsToggled == true)
            {
                Encuesta14dFiebreXaml.IsToggled = false;
                Encuesta14dTosXaml.IsToggled = false;
                Encuesta14dRespiracionXaml.IsToggled = false;
                Encuesta14dMalestarXaml.IsToggled = false;
                Encuesta14dOtroXaml.IsToggled = false;
            }
        }

        void switcher_ToggledHoySintomas(object sender, ToggledEventArgs e)
        {
            if (Encuesta14dNingunoXaml.IsToggled == true)
            {
                EncuestaHoyFiebreXaml.IsToggled = false;
                EncuestaHoyRespiracionXaml.IsToggled = false;
                EncuestaHoyMalestarXaml.IsToggled = false;
                EncuestaHoyNasalXaml.IsToggled = false;
                EncuestaHoyGarganteXaml.IsToggled = false;
                EncuestaHoyOtroXaml.IsToggled = false;
            }
        }

        async void OnButtonClicked(object sender, EventArgs args)
        {
            var current = Connectivity.NetworkAccess;

            if (current == NetworkAccess.Internet) // HAY Internet
            {

                Encuesta newEncuesta = new Encuesta
                {
                    UsuarioId = App.usuarioId,   // Gran aporte de Ivanov :)
                    EncuestaFecha = EncuestaFechaXaml.Date,
                    EncuestaNombre = EncuestaNombreXaml.Text,
                    EncuestaApellido = EncuestaApellidoXaml.Text,
                    EncuestaDocumentoTipo = EncuestaDocumentoTipoXaml.SelectedItem.ToString(),
                    EncuestaDocumentoNumero = EncuestaDocumentoXaml.Text,
                    EncuestaCelular = EncuestaCelularXaml.Text,
                    EncuestaEmpresa = EncuestaEmpresaXaml.SelectedItem.ToString(),
                    EncuestaAreaFisica = EncuestaAreaFisicaXaml.Text,

                    Encuesta14dViaje = Encuesta14dViajeXaml.IsToggled,
                    Encuesta14dContactoViaje = Encuesta14dContactoViajeXaml.IsToggled,
                    Encuesta14dPersona = Encuesta14dPersonaXaml.IsToggled,
                    Encuesta14dPersonaProb = Encuesta14dPersonaProbXaml.IsToggled,

                    Encuesta14dFiebre = Encuesta14dFiebreXaml.IsEnabled,
                    Encuesta14dTos = Encuesta14dTosXaml.IsEnabled,
                    Encuesta14dRespiracion = Encuesta14dRespiracionXaml.IsEnabled,
                    Encuesta14dMalestar = Encuesta14dMalestarLabel.IsEnabled,
                    Encuesta14dOtro = Encuesta14dOtroXaml.IsEnabled,
                    Encuesta14dNinguno = Encuesta14dNingunoXaml.IsEnabled,
                    Encuesta14dObservaciones = Encuesta14dObservacionesXaml.Text,
                    
                    EncuestaHoyFiebre = EncuestaHoyFiebreXaml.IsEnabled,
                    EncuestaHoyTos = EncuestaHoyTosXaml.IsEnabled,
                    EncuestaHoyRespiracion = EncuestaHoyRespiracionXaml.IsEnabled,
                    EncuestaHoyMalestar = EncuestaHoyMalestarXaml.IsEnabled,
                    EncuestaHoyNasal = EncuestaHoyNasalXaml.IsEnabled,
                    EncuestaHoyOtro = EncuestaHoyOtroXaml.IsEnabled,
                    EncuestaHoyNinguno = EncuestaHoyNingunoXaml.IsEnabled,
                    EncuestaHoyObservaciones = EncuestaHoyObservacionesXaml.Text,

                    EncuestaDeclaracion = EncuestaDeclaracionXaml.IsEnabled,
                    EncuestaEnfermedadBase = EncuestaEnfermedadBaseXaml.IsEnabled,
                    EncuestaEnfermedadBaseObservaciones = EncuestaEnfermedadBaseObservacionesXaml.Text,
                    EncuestaAdultoMayor = EncuestaAdultoMayorXaml.IsEnabled,
                    EncuestaAdultoMayorNumero = short.Parse(EncuestaAdultoMayoreNumeroXaml.Text)
                };

                WebApiHelper webApiHelper = new WebApiHelper();
                await webApiHelper.ApiPostEncuesta(newEncuesta);

                await DisplayAlert("Datos enviados", "De click en aceptar", "Aceptar");
                AppMasterDetail masterDetailMainPage = new AppMasterDetail();
                //MainPage mainPage = new MainPage();
                Application.Current.MainPage = new AppMasterDetail();
                //await NavigationPage.Push(masterDetailMainPage); 

            }
            else // NO HAY Internet
            {

                await DisplayAlert("Error para enviar datos", "No presenta conexón a internet", "Aceptar");

            }

        }
    }

}
