﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xamIvanovMaps001
{

    public class AppMasterDetailMasterMenuItem
    {
        public AppMasterDetailMasterMenuItem()
        {
            TargetType = typeof(AppMasterDetailMasterMenuItem);
        }
        public int Id { get; set; }
        public string Title { get; set; }

        public Type TargetType { get; set; }
    }
}