﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace xamIvanovMaps001.Models
{
    public class Constants
    {
        #region "Members"

        public bool isDev = true;
        public static Color BackgroundColor = Color.FromArgb(58, 153, 215);
        public static Color MainTextColor = Color.White;
        public static double LogoHeight = 128;

        #endregion "Members"
    }
}
