﻿using System;
using System.Collections.Generic;
using System.Text;

namespace xamIvanovMaps001.Models
{
    class Seguimiento
    {
        public long SeguimientoId { get; set; }
        public long UsuarioId { get; set; }
        public string SeguimientoFecha { get; set; }
        public Nullable<decimal> SeguimientoLat { get; set; }
        public Nullable<decimal> SeguimientoLon { get; set; }
    }
}
