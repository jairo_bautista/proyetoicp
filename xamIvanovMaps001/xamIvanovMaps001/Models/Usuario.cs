﻿using System;
using System.Collections.Generic;
using System.Text;

namespace xamIvanovMaps001.Models
{
    class Usuario
    {
        public long UsuarioId { get; set; }
        public string UsuarioUserName { get; set; }
        public string UsuarioPassword { get; set; }
        public string UsuarioNombre { get; set; }
        public string UsuarioApellido { get; set; }
        public string UsuarioDocumentoTipo { get; set; }
        public string UsuarioDocumentoNumero { get; set; }
        public string UsuarioCelular { get; set; }
        public string UsuarioEmail { get; set; }
        public string UsuarioEmpresa { get; set; }
        public string UsuarioAreaFisica { get; set; }
        public string UsuarioConfirmacion { get; set; }
        public Nullable<bool> UsuarioConfirmado { get; set; }
    }
}
