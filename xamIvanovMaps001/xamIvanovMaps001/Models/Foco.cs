﻿using System;
using System.Collections.Generic;
using System.Text;

namespace xamIvanovMaps001.Models
{
    class Foco
    {
        public long FocoId { get; set; }
        public Nullable<decimal> FocoLat { get; set; }
        public Nullable<decimal> FocoLon { get; set; }
        public Nullable<decimal> FotoSeveridad { get; set; }
    }
}
