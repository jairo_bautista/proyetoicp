﻿using System;
using System.Collections.Generic;
using System.Text;

namespace xamIvanovMaps001.Models
{
    class Encuesta
    {
        public long EncuestaId { get; set; }
        public long UsuarioId { get; set; }
        public Nullable<System.DateTime> EncuestaFecha { get; set; }
        public string EncuestaNombre { get; set; }
        public string EncuestaApellido { get; set; }
        public string EncuestaCedula { get; set; }
        public string EncuestaCelular { get; set; }
        public string EncuestaEmpresa { get; set; }
        public string EncuestaAreaFisica { get; set; }
        public Nullable<bool> Encuesta14dCiudad { get; set; }
        public Nullable<bool> Encuesta14dPersona { get; set; }
        public Nullable<bool> Encuesta14dPersonaProb { get; set; }
        public Nullable<bool> Encuesta14dFiebre { get; set; }
        public Nullable<bool> Encuesta14dTos { get; set; }
        public Nullable<bool> Encuesta14dRespiracion { get; set; }
        public Nullable<bool> Encuesta14dMalestar { get; set; }
        public Nullable<bool> Encuesta14dOtro { get; set; }
        public Nullable<bool> Encuesta14dNinguno { get; set; }
        public string Encuesta14dObservaciones { get; set; }
        public Nullable<bool> EncuestaHoyFiebre { get; set; }
        public Nullable<bool> EncuestaHoyTos { get; set; }
        public Nullable<bool> EncuestaHoyRespiracion { get; set; }
        public Nullable<bool> EncuestaHoyMalestar { get; set; }
        public Nullable<bool> EncuestaHoyNasal { get; set; }
        public Nullable<bool> EncuestaHoyGargante { get; set; }
        public Nullable<bool> EncuestaHoyOtro { get; set; }
        public Nullable<bool> EncuestaHoyNinguno { get; set; }
        public string EncuestaHoyObservaciones { get; set; }
        public Nullable<bool> EncuestaDeclaracion { get; set; }
        public string EncuestaDocumentoTipo { get; set; }
        public string EncuestaDocumentoNumero { get; set; }
        public Nullable<bool> Encuesta14dViaje { get; set; }
        public Nullable<bool> Encuesta14dContactoViaje { get; set; }
        public Nullable<bool> EncuestaEnfermedadBase { get; set; }
        public string EncuestaEnfermedadBaseObservaciones { get; set; }
        public Nullable<bool> EncuestaAdultoMayor { get; set; }
        public Nullable<short> EncuestaAdulroMayorNumero { get; set; }
        public Nullable<bool> EncuestaHoyGarganta { get; set; }
        public Nullable<short> EncuestaAdultoMayorNumero { get; set; }
    }
}
