﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using xamIvanovMaps001.Models;
using xamIvanovMaps001.Data;
using Xamarin.Essentials;
using xamivanovmaps001;

namespace xamIvanovMaps001
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
            initAdicional();
        }

        private void initAdicional()
        {
            // Look&Feel
            BackgroundColor = Constants.BackgroundColor;
            lblUserName.TextColor = Constants.MainTextColor;
            lblPassword.TextColor = Constants.MainTextColor;
            actiLogin.IsVisible = false;
            lblUserName.IsVisible = true;
            txtUserName.IsVisible = true;
            lblPassword.IsVisible = true;
            txtPassword.IsVisible = true;
            imgLogo.HeightRequest = Constants.LogoHeight;
            // Behaviors
            txtUserName.Completed += (s, e) => txtPassword.Focus();
            txtPassword.Completed += (s, e) => CmdIngresar_Clicked(s, e);
        }

        private async void CmdIngresar_Clicked(object sender, EventArgs e)
        {
            lblUserName.IsVisible = false;
            txtUserName.IsVisible = false;
            lblPassword.IsVisible = false;
            txtPassword.IsVisible = false;
            actiLogin.IsVisible = true;

            var current = Connectivity.NetworkAccess;
            User user = new User(txtUserName.Text, txtPassword.Text);

            if (current == NetworkAccess.Internet) // HAY Internet
            {
                lblConexion.TextColor = Color.Wheat;
                lblConexion.Text = "(BD central)";
                if (user.ValidarDatos())  // Casillas de texto completos
                {
                    WebApiHelper webApiHelper = new WebApiHelper();
                    await webApiHelper.ApiGetUsuario(txtUserName.Text, txtPassword.Text);
                    if(webApiHelper.IntUserId != 0)   // Encontró al usuario
                    {
                        //await DisplayAlert("Login Exitoso", "UsuarioId: " + webApiHelper.IntUserId, "Ok");
                        App.usuarioId = webApiHelper.IntUserId;
                        AppMasterDetail masterDetailMainPage = new AppMasterDetail();
                        //MainPage mainPage = new MainPage();
                        Application.Current.MainPage = new AppMasterDetail();
                        //await NavigationPage.Push(masterDetailMainPage); 
                    }
                    else   // NO Encontró al usuario
                    {
                        await DisplayAlert("Login Fallido", "UsuarioId: " + webApiHelper.IntUserId, "Ok");
                    }
                }
                else  // Casillas de texto NO completas
                {
                    await DisplayAlert("Login", "Error: UserName o Password vacío", "Ok");
                }
            }
            else // NO HAY Internet
            {
                lblConexion.TextColor = Color.Orange;
                lblConexion.Text = "(BD local)";
                if (user.ValidarDatos())  // Casillas de texto completos
                {
                    // Buscar UserName
                    User UserHallado = await App.SQLiteDb.GetItemByUserNameAsync(txtUserName.Text);
                    if (UserHallado != null)
                    {
                        // Verificar el password
                        if (txtPassword.Text == Crypto.Decrypt(UserHallado.Password, txtPassword.Text))
                        {
                            AppMasterDetail masterDetailMainPage = new AppMasterDetail();
                            //MainPage mainPage = new MainPage();
                            Application.Current.MainPage = new AppMasterDetail();
                            //await NavigationPage.Push(masterDetailMainPage); 
                        }
                        else
                        {
                            await DisplayAlert("Login", "Error: Usuario no reconocido", "Ok");
                        }
                    }
                    else
                    {
                        await DisplayAlert("Login", "Error: Usuario no hallado", "Ok");
                    }
                }
                else  // Casillas de texto NO completas
                {
                    await DisplayAlert("Login", "Error: UserName o Password vacío", "Ok");
                }
            }
            lblUserName.IsVisible = true;
            txtUserName.IsVisible = true;
            lblPassword.IsVisible = true;
            txtPassword.IsVisible = true;
            actiLogin.IsVisible = false;
            lblConexion.TextColor = Color.Wheat;
            lblConexion.Text = "";
        }

        private async void CmdRegistrarse_Clicked(object sender, EventArgs e)
        {
            Registro nextPage = new Registro(txtUserName.Text, txtPassword.Text);
            await this.Navigation.PushAsync(nextPage);

            //lblUserName.IsVisible = false;
            //txtUserName.IsVisible = false;
            //lblPassword.IsVisible = false;
            //txtPassword.IsVisible = false;
            //actiLogin.IsVisible = true;
            //User UserRepetido = await App.SQLiteDb.GetItemByUserNameAsync(txtUserName.Text);
            //if (UserRepetido == null)
            //{
            //    try
            //    {
            //        User userNuevo = new User(txtUserName.Text, Crypto.Encrypt(txtPassword.Text, txtPassword.Text));
            //        await App.SQLiteDb.SaveItemAsync(userNuevo);
            //        Usuario nuevoUsuario = new Usuario
            //        {
            //            UsuarioUserName = txtUserName.Text,
            //            UsuarioPassword = txtPassword.Text  
            //            //UsuarioPassword = Crypto.Encrypt(txtPassword.Text, txtPassword.Text)
            //        };
            //        WebApiHelper webApiHelper = new WebApiHelper();
            //        await webApiHelper.ApiPostUsuario(nuevoUsuario);
            //        await DisplayAlert("Login", "Registro exitoso!", "Ok");
            //    }
            //    catch (Exception ex)
            //    {
            //        await DisplayAlert("Login", ex.Message, "Ok");
            //        lblUserName.IsVisible = true;
            //        txtUserName.IsVisible = true;
            //        lblPassword.IsVisible = true;
            //        txtPassword.IsVisible = true;
            //        actiLogin.IsVisible = false;
            //    }
            //}
            //else
            //{
            //    await DisplayAlert("Login", "Error: Usuario ya existe", "Ok");
            //}
            //lblUserName.IsVisible = true;
            //txtUserName.IsVisible = true;
            //lblPassword.IsVisible = true;
            //txtPassword.IsVisible = true;
            //actiLogin.IsVisible = false;
        }
    }
}