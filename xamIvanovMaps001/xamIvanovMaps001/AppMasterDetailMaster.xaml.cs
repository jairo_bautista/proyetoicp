﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace xamIvanovMaps001
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AppMasterDetailMaster : ContentPage
    {
        public ListView ListView;

        public AppMasterDetailMaster()
        {
            InitializeComponent();

            BindingContext = new AppMasterDetailMasterViewModel();
            ListView = MenuItemsListView;
        }

        class AppMasterDetailMasterViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<AppMasterDetailMasterMenuItem> MenuItems { get; set; }

            public AppMasterDetailMasterViewModel()
            {
                MenuItems = new ObservableCollection<AppMasterDetailMasterMenuItem>(new[]
                {
                    new AppMasterDetailMasterMenuItem { Id = 0, Title = "Mapa", TargetType = typeof(MapaPage) },
                    new AppMasterDetailMasterMenuItem { Id = 1, Title = "Encuesta", TargetType = typeof(EncuestaPage) },
                    new AppMasterDetailMasterMenuItem { Id = 2, Title = "Encuesta XAML", TargetType = typeof(FormularioEncuesta)},
                    new AppMasterDetailMasterMenuItem { Id = 3, Title = "QRcode", TargetType = typeof(xamivanovmaps001.QRcode) },
                    //new AppMasterDetailMasterMenuItem { Id = 4, Title = "Pag 5" },
                });
            }

            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }
    }
}