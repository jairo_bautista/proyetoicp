﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using xamIvanovMaps001;
using xamIvanovMaps001.Data;
using xamIvanovMaps001.Models;

namespace xamivanovmaps001
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Registro : ContentPage
    {
        bool blnPasswordsIguales = false;
        string txtErrores = "";
        Usuario usuarioNuevo = new Usuario();

        public Registro(string UN, string PW)
        {
            InitializeComponent();
            actiLogin.IsVisible = false;
            var listTxts = stckEntries.Children.Where(c => c.GetType().ToString() != "Label");
            txtUserName.Text = UN;
            txtPassword.Text = PW;
        }

        private void pckDocumentoTipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            usuarioNuevo.UsuarioDocumentoTipo = pckDocumentoTipo.SelectedItem.ToString();
        }

        private void pckEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {
            usuarioNuevo.UsuarioEmpresa = pckEmpresa.SelectedItem.ToString();
        }

        private void TxtPassword_TextChanged(object sender, EventArgs e)
        {
            blnPasswordsIguales = txtPassword.Text == txtPasswordConfirmar.Text;
            if (blnPasswordsIguales)
            {
                lblPasswordError.TextColor = Color.DarkOliveGreen;
                lblPasswordError.Text = "OK! - Contraseñas iguales";

            }
            else
            {
                lblPasswordError.TextColor = Color.Red;
                lblPasswordError.Text = "ERROR! - Contraseñas diferentes";
            }
            lblPasswordError.IsVisible = true;
        }

        private bool blnFormularioCompleto()
        {
            txtErrores = "";
            if (String.IsNullOrEmpty(txtUserName.Text)) { txtErrores += "<br />Username está vacío"; };
            if (String.IsNullOrEmpty(txtPassword.Text)) { txtErrores += "<br />Password está vacío"; };
            if (String.IsNullOrEmpty(txtPasswordConfirmar.Text)) { txtErrores += "<br />Confirmación de Password está vacío"; };
            if (String.IsNullOrEmpty(txtNombre.Text)) { txtErrores += "<br />Nombre(s) está vacío"; };
            if (String.IsNullOrEmpty(txtApellido.Text)) { txtErrores += "<br />Apellido(s) está vacío"; };
            if (pckDocumentoTipo.SelectedItem == null) { txtErrores += "<br />Tipo de Documento no selecionado"; };
            if (String.IsNullOrEmpty(txtDocumentoNumero.Text)) { txtErrores += "<br />Número de Documento está vacío"; };
            if (String.IsNullOrEmpty(txtCelular.Text)) { txtErrores += "<br />Celular está vacío"; };
            if (String.IsNullOrEmpty(txtEmail.Text)) { txtErrores += "<br />Email está vacío"; };
            if (pckEmpresa.SelectedItem == null) { txtErrores += "<br />Empresa no selecionada"; };
            if (String.IsNullOrEmpty(txtAreaFisica.Text)) { txtErrores += "<br />Area Física está vacío"; };
            if (!blnPasswordsIguales) { txtErrores += "<br />Passwords desiguales"; };
            return (txtErrores == "");
        }

        private async void cmdRegistrarse_Clicked(object sender, EventArgs e)
        {
            actiLogin.IsVisible = true;
            if (blnFormularioCompleto())
            {
                //await DisplayAlert("Exito","Formulario Ok","Ok");
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet) // HAY Internet
                {
                    lblConexion.TextColor = Color.Wheat;
                    lblConexion.Text = "(BD central)";
                    WebApiHelper webApiHelper = new WebApiHelper();
                    await webApiHelper.ApiGetUsuario(txtUserName.Text, "");
                    if (webApiHelper.IntUserId == 0)    // No existe ese UserName (para evitar duplicados)
                    {
                        try
                        {
                            Usuario nuevoUsuario = new Usuario
                            {
                                UsuarioUserName = txtUserName.Text,
                                UsuarioPassword = txtPassword.Text,
                                UsuarioNombre = txtNombre.Text,
                                UsuarioApellido = txtApellido.Text,
                                UsuarioDocumentoTipo = pckDocumentoTipo.SelectedItem.ToString(),
                                UsuarioDocumentoNumero = txtDocumentoNumero.Text,
                                UsuarioCelular = txtCelular.Text,
                                UsuarioEmail = txtEmail.Text,
                                UsuarioEmpresa = pckEmpresa.SelectedItem.ToString(),
                                UsuarioAreaFisica = txtAreaFisica.Text
                                //UsuarioPassword = Crypto.Encrypt(txtPassword.Text, txtPassword.Text)
                            };
                            await webApiHelper.ApiPostUsuario(nuevoUsuario);
                            await DisplayAlert("Login", "Registro exitoso!", "Ok");
                            await Application.Current.MainPage.Navigation.PopAsync();
                        }
                        catch (Exception ex)
                        {
                            await DisplayAlert("Login", ex.Message, "Ok");
                            actiLogin.IsVisible = false;
                        }
                    }
                    else
                    {
                        await DisplayAlert("Login", "Error: Usuario ya existe", "Ok");
                    }
                    actiLogin.IsVisible = false;
                }
                else // NO HAY Internet
                {
                    await DisplayAlert("Registro", "Error: El Registro requiere conexión a Internet!", "Ok");
                    //lblConexion.TextColor = Color.Orange;
                    //lblConexion.Text = "(BD local)";
                    //actiLogin.IsVisible = true;
                    //User UserRepetido = await App.SQLiteDb.GetItemByUserNameAsync(txtUserName.Text);
                    //if (UserRepetido == null)
                    //{
                    //    try
                    //    {
                    //        Usuario nuevoUsuario = new Usuario
                    //        {
                    //            UsuarioUserName = txtUserName.Text,
                    //            UsuarioPassword = txtPassword.Text,
                    //            UsuarioNombre = txtNombre.Text,
                    //            UsuarioApellido = txtApellido.Text,
                    //            UsuarioDocumentoTipo = pckDocumentoTipo.SelectedItem.ToString(),
                    //            UsuarioDocumentoNumero = txtDocumentoNumero.Text,
                    //            UsuarioCelular = txtCelular.Text,
                    //            UsuarioEmail = txtEmail.Text,
                    //            UsuarioEmpresa = pckEmpresa.SelectedItem.ToString(),
                    //            UsuarioAreaFisica = txtAreaFisica.Text
                    //            //UsuarioPassword = Crypto.Encrypt(txtPassword.Text, txtPassword.Text)
                    //        };
                    //        await App.SQLiteDb.SaveItemAsync(nuevoUsuario);
                    //        await DisplayAlert("Login", "Registro exitoso!", "Ok");
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        await DisplayAlert("Login", ex.Message, "Ok");
                    //        actiLogin.IsVisible = false;
                    //    }
                    //}
                    //else
                    //{
                    //    await DisplayAlert("Login", "Error: Usuario ya existe", "Ok");
                    //}
                    actiLogin.IsVisible = false;
                }
            }
            else
            {
                await DisplayAlert("Error", "Formulario incompleto:" + System.Environment.NewLine + txtErrores.Replace("<br />", System.Environment.NewLine), "Ok");
            }
            actiLogin.IsVisible = false;
        }

    }
}