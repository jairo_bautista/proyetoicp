﻿using System;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using xamIvanovMaps001.Data;

namespace xamIvanovMaps001
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            // Para desprotegerla de git
            //MainPage = new MainPage();
            //MainPage = new LoginPage();
            MainPage = new NavigationPage(new LoginPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }

        static long usuarioId1;
        public static long usuarioId { get => usuarioId1; set => usuarioId1 = value; }

        static SQLiteHelper db;

        public static SQLiteHelper SQLiteDb
        {
            get
            {
                //File.Delete(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "XamarinSQLite.db3"));
                //Console.WriteLine(">>> *** Borré la BD %s", Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "XamarinSQLite.db3"));
                //db = new SQLiteHelper(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "XamarinSQLite.db3"));
                if (db == null)
                {
                    db = new SQLiteHelper(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "XamarinSQLite.db3"));
                }
                return db;
            }
        }

    }
}
