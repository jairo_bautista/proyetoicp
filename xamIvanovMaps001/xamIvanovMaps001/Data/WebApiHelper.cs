﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using xamIvanovMaps001.Models;
using System.Threading.Tasks;
using System.Diagnostics;

namespace xamIvanovMaps001.Data
{
    class WebApiHelper
    {
        private HttpClient httpClient = new HttpClient();

        private int intUserId;

        public int IntUserId { get => intUserId; set => intUserId = value; }

        public async Task ApiPostUsuario(Usuario newUsuario)
        {
            try
            {
                HttpResponseMessage response = null;
                var json = JsonConvert.SerializeObject(newUsuario);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                response = await httpClient.PostAsync("http://localhost:54699/api/Usuario", content);
                if (response.IsSuccessStatusCode)
                {
                    Debug.WriteLine(@"\tUsuario successfully saved.");
                }
                else
                {
                    Debug.WriteLine(response.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public async Task ApiPostSeguimiento(Seguimiento newSeguimiento)
        {
            try
            {
                httpClient.BaseAddress = new Uri("http://api.covirisk.adrianivanov.net");
                HttpResponseMessage response = null;
                var json = JsonConvert.SerializeObject(newSeguimiento);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                response = await httpClient.PostAsync("/api/Seguimiento", content);
                Console.WriteLine(response.Content.ReadAsStringAsync());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public async Task ApiPostEncuesta(Encuesta newEncuesta)
        {
            try
            {
                httpClient.BaseAddress = new Uri("http://api.covirisk.adrianivanov.net");
                HttpResponseMessage response = null;
                var json = JsonConvert.SerializeObject(newEncuesta);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                response = await httpClient.PostAsync("/api/EncuestaUy", content);
                Console.WriteLine(response.Content.ReadAsStringAsync());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public async Task ApiGetUsuario(string txtUser, string txtPassword)
        {
            try
            {
                HttpResponseMessage response = null;
                //string txtQueryString = "http://pruebas.adrianivanov.net/api/Usuario?U=" + txtUser + "&P=" + txtPassword;
                string txtQueryString = "http://api.covirisk.adrianivanov.net/api/Usuario?U=" + txtUser + "&P=" + txtPassword;
                response = await httpClient.GetAsync(txtQueryString);
                if (response.IsSuccessStatusCode)
                {
                    string txtUserId = await response.Content.ReadAsStringAsync();
                    intUserId = Int32.Parse(txtUserId);
                    Debug.WriteLine(@"\tUsuario successfully saved.");
                }
                else
                {
                    Debug.WriteLine(response.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

    }
}
