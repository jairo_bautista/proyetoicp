﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Essentials;
using Xamarin.Forms.Maps;
using Plugin.Geolocator;
using xamIvanovMaps001.Data;
using xamIvanovMaps001.Models;

namespace xamIvanovMaps001
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MapaPage : ContentPage
    {
        // Pines de prueba
        Pin pin1 = new Pin();
        Pin pin2 = new Pin();
        Pin pin3 = new Pin();
        Pin pin4 = new Pin();
        Pin pin5 = new Pin();
        // Las distancias se obtienen en Km. Distancias mayores a dblDistanciaSegura tienen Riesgo 0
        // Distancias menores a dblDistanciaSegura empiezan a tener un cierto riesgo.
        // Distancia igual a cero, tendrá el máximo Riesgo, que es 5.
        double dblDistanciaSegura = 0.5;    // Km
        // Calculador de Distancia promedio, Riesgo y pintador del mismo
        private void CalcularRiesgoYPintarlo(double dblLatUser, double dblLonUser)
        {
            // Calcular location's para Usuario y para Focos
            Location locUser = new Location(dblLatUser, dblLonUser);
            Location locPin1 = new Location(pin1.Position.Latitude, pin1.Position.Longitude);
            Location locPin2 = new Location(pin2.Position.Latitude, pin2.Position.Longitude);
            Location locPin3 = new Location(pin3.Position.Latitude, pin3.Position.Longitude);
            Location locPin4 = new Location(pin4.Position.Latitude, pin4.Position.Longitude);
            Location locPin5 = new Location(pin5.Position.Latitude, pin5.Position.Longitude);
            // Hallar la menor distancia, el Pin más cercano
            double dblDistanciaAPin1 = Location.CalculateDistance(locUser, locPin1, DistanceUnits.Kilometers); ;
            double dblDistanciaMenor = dblDistanciaAPin1;
            double dblDistanciaAPin2 = Location.CalculateDistance(locUser, locPin2, DistanceUnits.Kilometers);
            if(dblDistanciaMenor > dblDistanciaAPin2) { dblDistanciaMenor = dblDistanciaAPin2; }
            double dblDistanciaAPin3 = Location.CalculateDistance(locUser, locPin3, DistanceUnits.Kilometers);
            if (dblDistanciaMenor > dblDistanciaAPin3) { dblDistanciaMenor = dblDistanciaAPin3; }
            double dblDistanciaAPin4 = Location.CalculateDistance(locUser, locPin4, DistanceUnits.Kilometers);
            if (dblDistanciaMenor > dblDistanciaAPin4) { dblDistanciaMenor = dblDistanciaAPin4; }
            double dblDistanciaAPin5 = Location.CalculateDistance(locUser, locPin5, DistanceUnits.Kilometers);
            if (dblDistanciaMenor > dblDistanciaAPin5) { dblDistanciaMenor = dblDistanciaAPin5; }
            // Si dblDistanciaMenor es 0, el Riesgo es 5, el más alto.
            // Si es >= dblDistanciaSegura (Km) el Riesgo es 0, mínimo.
            double dblValorRiesgo = 5 * ( (dblDistanciaSegura - dblDistanciaMenor) / dblDistanciaSegura) ;
            if (dblValorRiesgo > 5)
            {
                dblValorRiesgo = 5;
            }
            if (dblValorRiesgo < 0)
            {
                dblValorRiesgo = 0;
            }
            sldNivelDeRiesgo.Value = dblValorRiesgo;
            lblValorRiesgo.Text = sldNivelDeRiesgo.Value.ToString();
        }
        // Coordenadas del Usuario
        double dblUsuarioLat = 0;
        double dblUsuarioLon = 0;
        // Clase helper para interactuar con la WebAPI de la base de datos remota
        WebApiHelper webApiHelper = new WebApiHelper();

        public MapaPage()
        {
            InitializeComponent();
            obtenerUbicacion();
            seguirUbicacion();
        }

        private async void obtenerUbicacion()
        {
            try
            {
                mapMapa.IsShowingUser = true;
                var request = new GeolocationRequest(GeolocationAccuracy.Medium);
                Location location = await Geolocation.GetLocationAsync(request);

                if (location != null)
                {
                    dblUsuarioLat = location.Latitude;
                    dblUsuarioLon = location.Longitude;
                    Position position = new Position(dblUsuarioLat, dblUsuarioLon);
                    mapMapa.MoveToRegion(new MapSpan(position, 0.01, 0.01));
                    // Crear pines n posiciones relativas al usuario
                    pin1 = new Pin
                    {
                        Label = "Reportado001",
                        Address = "Alto foco de contagio",
                        Type = PinType.Place,
                        Position = new Position(dblUsuarioLat - 0.003, dblUsuarioLon + 0.004)
                    };
                    pin2 = new Pin
                    {
                        Label = "Reportado002",
                        Address = "Bajo foco de contagio",
                        Type = PinType.Place,
                        Position = new Position(dblUsuarioLat + 0.002, dblUsuarioLon + 0.002)
                    };
                    pin3 = new Pin
                    {
                        Label = "Reportado003",
                        Address = "Medio foco de contagio",
                        Type = PinType.Place,
                        Position = new Position(dblUsuarioLat + 0.001, dblUsuarioLon - 0.003)
                    };
                    pin4 = new Pin
                    {
                        Label = "Reportado004",
                        Address = "Bajo foco de contagio",
                        Type = PinType.Place,
                        Position = new Position(dblUsuarioLat - 0.005, dblUsuarioLon - 0.001)
                    };
                    pin5 = new Pin
                    {
                        Label = "Reportado005",
                        Address = "Medio foco de contagio",
                        Type = PinType.Place,
                        Position = new Position(dblUsuarioLat + 0.005, dblUsuarioLon - 0.002)
                    };
                    // Pintar los pines en el mapa
                    mapMapa.Pins.Add(pin1);
                    mapMapa.Pins.Add(pin2);
                    mapMapa.Pins.Add(pin3);
                    mapMapa.Pins.Add(pin4);
                    mapMapa.Pins.Add(pin5);
                    CalcularRiesgoYPintarlo(dblUsuarioLat, dblUsuarioLon);
                    decimal decUsuarioLat = (decimal)dblUsuarioLat;
                    decimal decUsuarioLon = (decimal)dblUsuarioLon;
                    //Seguimiento nuevoSeguimiento = new Seguimiento
                    //{
                    //    UsuarioId = 1,
                    //    SeguimientoFecha = DateTime.Now.ToString(),
                    //    SeguimientoLat = decUsuarioLat,
                    //    SeguimientoLon = decUsuarioLon
                    //};
                    var datEsteInstante = DateTime.Now;
                    var txtAnio = datEsteInstante.Year;
                    var txtMes = datEsteInstante.Month.ToString().PadLeft(2, '0');
                    var txtDia = datEsteInstante.Day.ToString().PadLeft(2, '0'); ;
                    var txtHora = datEsteInstante.Hour.ToString().PadLeft(2, '0'); ;
                    var txtMinutos = datEsteInstante.Minute.ToString().PadLeft(2, '0'); ;
                    var txtSegundos = datEsteInstante.Second.ToString().PadLeft(2, '0'); ;
                    var txtEsteInstante = txtAnio + "-" + txtMes + " " + txtDia + "T" + txtHora + ":" + txtMinutos + ":" + txtSegundos;
                    Seguimiento nuevoSeguimiento = new Seguimiento
                    {
                        UsuarioId = 1,
                        SeguimientoFecha = txtEsteInstante,
                        SeguimientoLat = decUsuarioLat,
                        SeguimientoLon = decUsuarioLon
                    };
                    WebApiHelper webApiHelper = new WebApiHelper();
                    await webApiHelper.ApiPostSeguimiento(nuevoSeguimiento);
                }
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                await DisplayAlert("Jodidos", fnsEx.Message, "Jodidos");
                // Handle not supported on device exception
            }
            catch (FeatureNotEnabledException fneEx)
            {
                await DisplayAlert("Jodidos", fneEx.Message, "Jodidos");
                // Handle not enabled on device exception
            }
            catch (PermissionException pEx)
            {
                await DisplayAlert("Jodidos", pEx.Message, "Jodidos");
                // Handle permission exception
            }
            catch (Exception ex)
            {
                await DisplayAlert("Jodidos", ex.Message, "Jodidos");
                // Unable to get location
            }

        }

        private async void seguirUbicacion()
        {
            mapMapa.IsShowingUser = true;
            var locator = CrossGeolocator.Current;
            locator.DesiredAccuracy = 50;
            if(locator.IsGeolocationAvailable)
            {
                if(locator.IsGeolocationEnabled)
                {
                    if(locator.IsListening)
                    {
                        //var position = await locator.GetPositionAsync();
                        await locator.StartListeningAsync(new TimeSpan(0, 0, 5), 20);
                        //var loc = await locator.GetPositionAsync(TimeSpan.FromSeconds(20), null, true);
                        //Position position = new Position(loc.Latitude, loc.Longitude);
                        //mapMapa.MoveToRegion(new MapSpan(position, 0.01, 0.01));
                    }
                    locator.PositionChanged += async (cambio, args) =>
                    {
                        var location = args.Position;
                        Position position = new Position(location.Latitude, location.Longitude);
                        mapMapa.MoveToRegion(new MapSpan(position, 0.01, 0.01));
                        CalcularRiesgoYPintarlo(location.Latitude, location.Longitude);
                        decimal decUsuarioLat = (decimal)location.Latitude;
                        decimal decUsuarioLon = (decimal)location.Longitude;
                        //Seguimiento nuevoSeguimiento = new Seguimiento
                        //{
                        //    UsuarioId = 1,
                        //    SeguimientoFecha = DateTime.Now.ToString(),
                        //    SeguimientoLat = decUsuarioLat,
                        //    SeguimientoLon = decUsuarioLon
                        //};
                        var datEsteInstante = DateTime.Now;
                        var txtAnio = datEsteInstante.Year;
                        var txtMes = datEsteInstante.Month.ToString().PadLeft(2, '0');
                        var txtDia = datEsteInstante.Day.ToString().PadLeft(2, '0'); ;
                        var txtHora = datEsteInstante.Hour.ToString().PadLeft(2, '0'); ;
                        var txtMinutos = datEsteInstante.Minute.ToString().PadLeft(2, '0'); ;
                        var txtSegundos = datEsteInstante.Second.ToString().PadLeft(2, '0'); ;
                        var txtEsteInstante = txtAnio + "-" + txtMes + " " + txtDia + "T" + txtHora + ":" + txtMinutos + ":" + txtSegundos;
                        Seguimiento nuevoSeguimiento = new Seguimiento
                        {
                            UsuarioId = 1,
                            SeguimientoFecha = txtEsteInstante,
                            SeguimientoLat = decUsuarioLat,
                            SeguimientoLon = decUsuarioLon
                        };
                        WebApiHelper webApiHelper = new WebApiHelper();
                        await webApiHelper.ApiPostSeguimiento(nuevoSeguimiento);
                    };
                }
            }
        }
    }
}
