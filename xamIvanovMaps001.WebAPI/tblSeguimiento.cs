//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace xamIvanovMaps001.WebAPI
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblSeguimiento
    {
        public long SeguimientoId { get; set; }
        public long UsuarioId { get; set; }
        public Nullable<System.DateTime> SeguimientoFecha { get; set; }
        public Nullable<decimal> SeguimientoLat { get; set; }
        public Nullable<decimal> SeguimientoLon { get; set; }
    }
}
