﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using xamIvanovMaps001.WebAPI;

namespace xamIvanovMaps001.WebAPI.Controllers
{
    public class EncuestaController : ApiController
    {
        private db_a196b9_coviriskEntities db = new db_a196b9_coviriskEntities();

        // GET: api/Encuesta
        public IQueryable<tblEncuesta> GettblEncuesta()
        {
            return db.tblEncuesta;
        }

        // GET: api/Encuesta/5
        [ResponseType(typeof(tblEncuesta))]
        public async Task<IHttpActionResult> GettblEncuesta(long id)
        {
            tblEncuesta tblEncuesta = await db.tblEncuesta.FindAsync(id);
            if (tblEncuesta == null)
            {
                return NotFound();
            }

            return Ok(tblEncuesta);
        }

        // PUT: api/Encuesta/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PuttblEncuesta(long id, tblEncuesta tblEncuesta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tblEncuesta.EncuestaId)
            {
                return BadRequest();
            }

            db.Entry(tblEncuesta).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tblEncuestaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Encuesta
        [ResponseType(typeof(tblEncuesta))]
        public async Task<IHttpActionResult> PosttblEncuesta(tblEncuesta tblEncuesta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.tblEncuesta.Add(tblEncuesta);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (tblEncuestaExists(tblEncuesta.EncuestaId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = tblEncuesta.EncuestaId }, tblEncuesta);
        }

        // DELETE: api/Encuesta/5
        [ResponseType(typeof(tblEncuesta))]
        public async Task<IHttpActionResult> DeletetblEncuesta(long id)
        {
            tblEncuesta tblEncuesta = await db.tblEncuesta.FindAsync(id);
            if (tblEncuesta == null)
            {
                return NotFound();
            }

            db.tblEncuesta.Remove(tblEncuesta);
            await db.SaveChangesAsync();

            return Ok(tblEncuesta);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblEncuestaExists(long id)
        {
            return db.tblEncuesta.Count(e => e.EncuestaId == id) > 0;
        }
    }
}