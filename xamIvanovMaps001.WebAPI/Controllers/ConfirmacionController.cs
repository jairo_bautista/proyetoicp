﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using xamIvanovMaps001.WebAPI;

namespace xamIvanovMaps001.WebAPI.Controllers
{
    public class ConfirmacionController : ApiController
    {
        private db_a196b9_coviriskEntities db = new db_a196b9_coviriskEntities();

        // GET: api/Confirmacion
        //public IQueryable<tblConfirmacion> GettblConfirmacion()
        //{
        //    return db.tblConfirmacion;
        //}

        // GET: api/Usuario<?U=-username_&P=_password_>
        public async Task<HttpResponseMessage> GettblConfirmacionAsync(string G = "user")
        {
            if (G == "user")   // Si viene vacío...
            {
                // Responde como respondía la antigua... mejor no, porque traía toda la tabla.
                //return Request.CreateResponse(HttpStatusCode.OK, db.tblConfirmacion.ToList());
                // Reponde con un parco status OK y sin datos.
                return Request.CreateResponse(HttpStatusCode.OK);
            }

            else    // Si hay valores en los parámetros de la QueryString...
            {
                tblUsuario tblUsuario = db.tblUsuario.Where(u => u.UsuarioConfirmacion == G).FirstOrDefault();
                if (tblUsuario != null)
                {
                    tblUsuario.UsuarioConfirmado = true;
                    await db.SaveChangesAsync();
                    return Request.CreateResponse(HttpStatusCode.OK, "CoviRisk: Usuario Activado.");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "CoviRisk: ERROR: Usuario No hallado.");
                }
            }
        }


        // GET: api/Confirmacion/5
        [ResponseType(typeof(tblConfirmacion))]
        public async Task<IHttpActionResult> GettblConfirmacion(long id)
        {
            tblConfirmacion tblConfirmacion = await db.tblConfirmacion.FindAsync(id);
            if (tblConfirmacion == null)
            {
                return NotFound();
            }

            return Ok(tblConfirmacion);
        }

        // PUT: api/Confirmacion/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PuttblConfirmacion(long id, tblConfirmacion tblConfirmacion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tblConfirmacion.ConfirmacionId)
            {
                return BadRequest();
            }

            db.Entry(tblConfirmacion).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tblConfirmacionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Confirmacion
        [ResponseType(typeof(tblConfirmacion))]
        public async Task<IHttpActionResult> PosttblConfirmacion(tblConfirmacion tblConfirmacion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.tblConfirmacion.Add(tblConfirmacion);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = tblConfirmacion.ConfirmacionId }, tblConfirmacion);
        }

        // DELETE: api/Confirmacion/5
        [ResponseType(typeof(tblConfirmacion))]
        public async Task<IHttpActionResult> DeletetblConfirmacion(long id)
        {
            tblConfirmacion tblConfirmacion = await db.tblConfirmacion.FindAsync(id);
            if (tblConfirmacion == null)
            {
                return NotFound();
            }

            db.tblConfirmacion.Remove(tblConfirmacion);
            await db.SaveChangesAsync();

            return Ok(tblConfirmacion);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblConfirmacionExists(long id)
        {
            return db.tblConfirmacion.Count(e => e.ConfirmacionId == id) > 0;
        }
    }
}