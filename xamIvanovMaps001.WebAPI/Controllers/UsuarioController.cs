﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using xamIvanovMaps001.WebAPI;
using xamIvanovMaps001.WebAPI.Servicios;

namespace xamIvanovMaps001.WebAPI.Controllers
{
    public class UsuarioController : ApiController
    {
        private db_a196b9_coviriskEntities db = new db_a196b9_coviriskEntities();

        //// GET: api/Usuario
        //public IQueryable<tblUsuario> GettblUsuario()
        //{
        //    return db.tblUsuario;
        //}

        // GET: api/Usuario<?U=-username_&P=_password_>
        public HttpResponseMessage GettblUsuario(string U = "user", string P = "pass")
        {
            if (U == "user" & P == "pass")   // Si vienen vacíos U y P...
            {
                // Responde como respondía la antigua
                return Request.CreateResponse(HttpStatusCode.OK, db.tblUsuario.ToList());
            }
            else    // Si hay valores en los parámetros de la QueryString...
            {
                if (U != "user" & P == "pass")   // Si viene sólo U (preguntando si ya hay un Username así)
                {
                    tblUsuario UsuarioDuplicado = db.tblUsuario.Where(u => u.UsuarioUserName == U).FirstOrDefault();
                    if (UsuarioDuplicado != null) // Si ya hay uno...
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, UsuarioDuplicado.UsuarioId);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.NotFound, 0);
                    }
                }
                if (U != "user" & P != "pass")   // Si vienen U y P (proceso de Login)
                {
                    // Buscamos en la BD el Usuario con UserName = U y Password = P
                    tblUsuario UsuarioBuscado = db.tblUsuario.Where(u => u.UsuarioUserName == U && u.UsuarioPassword == P).FirstOrDefault();
                    if (UsuarioBuscado != null & UsuarioBuscado.UsuarioConfirmado == true) // Si lo encuentra Y ya está confirmado
                    {
                        // Devuelve el UsuarioId del Usuario
                        return Request.CreateResponse(HttpStatusCode.OK, UsuarioBuscado.UsuarioId);
                    }
                    else    // Si NO lo encuentra...
                    {
                        return Request.CreateResponse(HttpStatusCode.NotFound);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }


        // GET: api/Usuario/5
        [ResponseType(typeof(tblUsuario))]
        public async Task<IHttpActionResult> GettblUsuario(long id)
        {
            tblUsuario tblUsuario = await db.tblUsuario.FindAsync(id);
            if (tblUsuario == null)
            {
                return NotFound();
            }

            return Ok(tblUsuario);
        }

        // PUT: api/Usuario/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PuttblUsuario(long id, tblUsuario tblUsuario)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tblUsuario.UsuarioId)
            {
                return BadRequest();
            }

            db.Entry(tblUsuario).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tblUsuarioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Usuario
        [ResponseType(typeof(tblUsuario))]
        public async Task<IHttpActionResult> PosttblUsuario(tblUsuario tblUsuario)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // Generar un GUID
            string guidUsuarioConfirmacion = Guid.NewGuid().ToString("N");
            // agregar el GUID al nuevo usuario
            tblUsuario.UsuarioConfirmacion = guidUsuarioConfirmacion;
            // agregar el estado al nuevo usuario
            tblUsuario.UsuarioConfirmado = false;

            // Insertar el nuevo usuario (aún no confirmado)
            db.tblUsuario.Add(tblUsuario);

            // Insertar una nueva Confirmación
            db.tblConfirmacion.Add(new tblConfirmacion
            {
                ConfirmacionConfirmacion = guidUsuarioConfirmacion
            });

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (tblUsuarioExists(tblUsuario.UsuarioId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            // Enviar Email de confirmación en respuesta al registro el usuario
            var toAddress = tblUsuario.UsuarioEmail;
            var fromAddress = "webmaster@adrianivanov.net";
            var subject = "CoviRisk :: Email de confirmación de Registro (" + tblUsuario.UsuarioNombre + " " + tblUsuario.UsuarioApellido + ")";
            var message = new StringBuilder();
            message.Append("Apreciado/a<br />");
            message.Append(tblUsuario.UsuarioNombre + " " + tblUsuario.UsuarioApellido + "<br /><br />");
            message.Append("Usted se registró en la App CoviRisk usando los siguientes datos:<br />");
            message.Append("Username: " + tblUsuario.UsuarioUserName + "<br />");
            message.Append("Password: " + tblUsuario.UsuarioPassword + "<br />");
            message.Append("Email: " + tblUsuario.UsuarioEmail + "<br />");
            message.Append("Celular: " + tblUsuario.UsuarioCelular + "<br /><br />");
            message.Append("Para confirmar su cuenta, por favor haga clic en este Link:<br /><br />");
            message.Append("http://api.covirisk.adrianivanov.net/api/Confirmacion/?G=" + guidUsuarioConfirmacion + " <br /><br />");
            message.Append("Gracias por su atención.<br />");
            message.Append("El Equipo de CoviRisk.<br />");

            //start email Thread
            Servicios.Email email = new Servicios.Email();
            Thread tEmail = new Thread(() => email.SendEmail(toAddress, fromAddress, subject, message.ToString()));
            tEmail.Start();

            return CreatedAtRoute("DefaultApi", new { id = tblUsuario.UsuarioId }, tblUsuario);
        }

        // DELETE: api/Usuario/5
        [ResponseType(typeof(tblUsuario))]
        public async Task<IHttpActionResult> DeletetblUsuario(long id)
        {
            tblUsuario tblUsuario = await db.tblUsuario.FindAsync(id);
            if (tblUsuario == null)
            {
                return NotFound();
            }

            db.tblUsuario.Remove(tblUsuario);
            await db.SaveChangesAsync();

            return Ok(tblUsuario);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblUsuarioExists(long id)
        {
            return db.tblUsuario.Count(e => e.UsuarioId == id) > 0;
        }
    }
}