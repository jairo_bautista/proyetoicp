﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using xamIvanovMaps001.WebAPI;

namespace xamIvanovMaps001.WebAPI.Controllers
{
    public class FocoController : ApiController
    {
        private db_a196b9_coviriskEntities db = new db_a196b9_coviriskEntities();

        // GET: api/Foco
        public IQueryable<tblFoco> GettblFoco()
        {
            return db.tblFoco;
        }

        // GET: api/Foco/5
        [ResponseType(typeof(tblFoco))]
        public async Task<IHttpActionResult> GettblFoco(long id)
        {
            tblFoco tblFoco = await db.tblFoco.FindAsync(id);
            if (tblFoco == null)
            {
                return NotFound();
            }

            return Ok(tblFoco);
        }

        // PUT: api/Foco/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PuttblFoco(long id, tblFoco tblFoco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tblFoco.FocoId)
            {
                return BadRequest();
            }

            db.Entry(tblFoco).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tblFocoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Foco
        [ResponseType(typeof(tblFoco))]
        public async Task<IHttpActionResult> PosttblFoco(tblFoco tblFoco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.tblFoco.Add(tblFoco);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (tblFocoExists(tblFoco.FocoId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = tblFoco.FocoId }, tblFoco);
        }

        // DELETE: api/Foco/5
        [ResponseType(typeof(tblFoco))]
        public async Task<IHttpActionResult> DeletetblFoco(long id)
        {
            tblFoco tblFoco = await db.tblFoco.FindAsync(id);
            if (tblFoco == null)
            {
                return NotFound();
            }

            db.tblFoco.Remove(tblFoco);
            await db.SaveChangesAsync();

            return Ok(tblFoco);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblFocoExists(long id)
        {
            return db.tblFoco.Count(e => e.FocoId == id) > 0;
        }
    }
}