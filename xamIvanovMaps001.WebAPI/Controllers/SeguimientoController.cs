﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using xamIvanovMaps001.WebAPI;

namespace xamIvanovMaps001.WebAPI.Controllers
{
    public class SeguimientoController : ApiController
    {
        private db_a196b9_coviriskEntities db = new db_a196b9_coviriskEntities();

        // GET: api/Seguimiento
        public IQueryable<tblSeguimiento> GettblSeguimiento()
        {
            return db.tblSeguimiento;
        }

        // GET: api/Seguimiento/5
        [ResponseType(typeof(tblSeguimiento))]
        public async Task<IHttpActionResult> GettblSeguimiento(long id)
        {
            tblSeguimiento tblSeguimiento = await db.tblSeguimiento.FindAsync(id);
            if (tblSeguimiento == null)
            {
                return NotFound();
            }

            return Ok(tblSeguimiento);
        }

        // PUT: api/Seguimiento/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PuttblSeguimiento(long id, tblSeguimiento tblSeguimiento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tblSeguimiento.SeguimientoId)
            {
                return BadRequest();
            }

            db.Entry(tblSeguimiento).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tblSeguimientoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Seguimiento
        [ResponseType(typeof(tblSeguimiento))]
        public async Task<IHttpActionResult> PosttblSeguimiento(tblSeguimiento tblSeguimiento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.tblSeguimiento.Add(tblSeguimiento);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (tblSeguimientoExists(tblSeguimiento.SeguimientoId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = tblSeguimiento.SeguimientoId }, tblSeguimiento);
        }

        // DELETE: api/Seguimiento/5
        [ResponseType(typeof(tblSeguimiento))]
        public async Task<IHttpActionResult> DeletetblSeguimiento(long id)
        {
            tblSeguimiento tblSeguimiento = await db.tblSeguimiento.FindAsync(id);
            if (tblSeguimiento == null)
            {
                return NotFound();
            }

            db.tblSeguimiento.Remove(tblSeguimiento);
            await db.SaveChangesAsync();

            return Ok(tblSeguimiento);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblSeguimientoExists(long id)
        {
            return db.tblSeguimiento.Count(e => e.SeguimientoId == id) > 0;
        }
    }
}